import React, { Component, Fragment } from 'react';
import moment from 'moment-timezone';

import Modal from './Modal';
import Form from './Form';

import Shifts from './data/shifts';
import Config from './data/config';
import Roles from './data/roles';
import Employees from './data/employees';

import './App.css';

/*
  Hi, Sorry only had around two hours to complete this so its not my best work but hopefully it will
  do to showcase my ability. 

  Main things I didn't have time for was proper styling and creating a time input that is user friendly for the visual display.
*/

const GetData = () => { // This would be replaced with engine calls
  const RolesObject = Roles.reduce((Sum, Role) => Object.assign({}, Sum, {
    [Role.id]: Role,
  }), {});

  const EmployeeShifts =  Shifts.reduce((Sum, Shift) => ({ // Shifts by Employee ID
    ...Sum,
    [Shift.employee_id]: [
      ...(Sum[Shift.employee_id] || []),
      {
        ...Shift,
        role: RolesObject[Shift.role_id],
      }
    ]
  }), {});

  return Promise.resolve(Employees.map(Employee => Object.assign({}, Employee, {
    shifts: EmployeeShifts[Employee.id],
  })));
};

const GetMoment = Init => moment(Init).tz(Config.timezone);
const WeekStart = moment.tz('2018-06-18 00:00', Config.timezone); // What is the first day of the week or data set

class App extends Component {
  state = {
    EmployeeData: null,
    Edit: null,
    DisplayTable: true,
  };

  componentDidMount() { 
     // This Promise doesnt actually do anything but It would be async in real world
    GetData().then(EmployeeData => this.setState({
      EmployeeData,
    }));
  }

  handleEditClick = (EmployeeId, ShiftId) => event => { // When an Item is selected in the Visual interface
    this.EditModal.openModal()
    this.setState({
      Edit: {
        EmployeeId, 
        ShiftId,
        Shift: this.state.EmployeeData.find(X => X.id === EmployeeId).shifts.find(X => X.id === ShiftId),
      }
    })
  }

  toggleDispalyView = event => this.setState(PrevState => ({
    DisplayTable: !PrevState.DisplayTable,
  }));

  handleEditChange = event => this.setState(PrevState => ({
    Edit: {
      ...PrevState.Edit,
      Shift: {
        ...PrevState.Edit.Shift,
        ...event.target.value,
      }
    }
  }))

  handleSubmitEdit = () => {
    const PrevEmployeeIndex = this.state.EmployeeData.findIndex(X => X.id === this.state.Edit.EmployeeId);
    const OtherShifts = this.state.EmployeeData[PrevEmployeeIndex].shifts.filter(X => X.id !== this.state.Edit.ShiftId)
    const Employees = this.state.EmployeeData;

    Employees[PrevEmployeeIndex].shifts =[
      ...OtherShifts,
      this.state.Edit.Shift,
    ]

    this.setState({
      EmployeeData: Employees,
      Edit: null,
    });

    this.EditModal.closeModal();
  }

  handleTableEdit = EmployeeID => ShiftId => Field => event => { // Directly edits rather than a save buffer
    const PrevEmployeeIndex = this.state.EmployeeData.findIndex(X => X.id === EmployeeID);
    const PrevShiftIndex = this.state.EmployeeData[PrevEmployeeIndex].shifts.findIndex(X => X.id === ShiftId);
    const OtherShifts = this.state.EmployeeData[PrevEmployeeIndex].shifts.filter(X => X.id !== ShiftId)
    const Employees = this.state.EmployeeData;
    const NewTime = GetMoment(Employees[PrevEmployeeIndex].shifts[PrevShiftIndex][Field]);

    let NewHour = parseInt(event.target.value.split(':')[0], 10);
    let NewMins = parseInt(event.target.value.split(':')[1].slice(0, 2), 10);
    let Miridian = (event.target.value.slice(-2)).toLowerCase();

    if (Miridian !== 'am' && Miridian !== 'pm') {
      Miridian = 'am';
    }

    if (NewHour <= 12 && Miridian === 'pm') {
      NewHour = NewHour + 12;
    }

    NewTime.hour(NewHour); // Set Hour in timestmape
    NewTime.minutes(NewMins); // Set Minutes in timestamp

    if (NewTime.isValid()) {
      Employees[PrevEmployeeIndex].shifts =[
        ...OtherShifts,
        {
          ...Employees[PrevEmployeeIndex].shifts[PrevShiftIndex],
          [Field]: NewTime.toISOString(),
        }
      ]

      this.setState({
        EmployeeData: Employees,
      });
    }
  }

  renderVisual() {
    const Scalar = .01; // Scale for roster area

    const getRosterStyling = RosterEntry => { // Styling for a particular shift
      const StartUnix = GetMoment(RosterEntry.start_time).unix();
      const EndUnix = GetMoment(RosterEntry.end_time).unix();

      return {
        left: Scalar*(StartUnix - WeekStart.unix()),
        width: Scalar*(EndUnix - StartUnix),
        color: RosterEntry.role.text_colour,
        background: RosterEntry.role.background_colour,
      }
    };

    const LatestShiftUnix = Shifts.reduce((Latest, Shift) => { // The unix timestamp of the last shift in data set
      const Unix = GetMoment(Shift.end_time).unix();

      if (Latest < Unix) {
        return Unix;
      }

      return Latest;
    }, 0);

    const GetRowWidth = () => { // Width of the Roster Canvas
      return {
        width: Math.ceil((LatestShiftUnix - WeekStart.unix())*Scalar) + 'px',
      }
    }

    const RenderHeaderTimestamps = () => {
      const NumberDays = (LatestShiftUnix - WeekStart.unix())/(60*60*24);
      const DayWidth = (60*60*24)*Scalar;
      const StartPoint = WeekStart.clone(); // For some reason .add is a mutative function ?

      return (
        <Fragment>
          {(Array.apply(null, Array(Math.floor(NumberDays)))).map((_, Index) => (
            <div style={{
              width: DayWidth,
              left: DayWidth*Index,
            }} className='HeaderEntry'>
              <div className="WordWrapper">
                {StartPoint.add(1, 'day').format('dddd')}
              </div>
              <div className="HeaderMarker" style={{
                height: 50*Employees.length + 'px',
                top: '50px',
              }} />
            </div>  
          ))}
        </Fragment>
      )
    }

    const RenderModalContents = () => {
      if (this.state.Edit !== null) {
        const Employee = this.state.EmployeeData.find(X => X.id === this.state.Edit.EmployeeId);

        return (
          <div key={'Edit_' + this.state.Edit.EmployeeId + '_' + this.state.Edit.ShiftId}>
            <h4>{"Editing: " + Employee.first_name + "'s shift"}</h4>
            <Form defaultValue={this.state.Edit.Shift} onChange={this.handleEditChange}>
              <input className='ModalInput' type="text" name="start_time" />
              <input className='ModalInput' type="text" name="end_time" />
              <button onClick={this.handleSubmitEdit}>Save</button>
            </Form>
          </div>
        );
      }

      return null;
    }

    return (
      <div className="App">
        <Modal startOpen={false} onRef={ref => (this.EditModal = ref)}>
          {RenderModalContents()}
        </Modal>
        <div className="EmployeeRow">
          <div className="ContactContainer">
            <div className="EmployeRow__Header EmployeeRow_Row">
              <div className="Employee__Contact">
              </div>
            </div>
            {this.state.EmployeeData.map(Employee => (
              <div className="EmployeeRow_Row" key={"EmployeeContactRow__" + Employee.id}>
                <div className="Employee__Contact">
                  {Employee.first_name + " " + Employee.last_name}
                </div>
              </div>
            ))}
          </div>
          <div className="RosterContainer">
            <div className="InnerRosterContainer" style={GetRowWidth()}>
              <div className="EmployeRow__Header EmployeeRow_Row">
                {RenderHeaderTimestamps()}
              </div>
              {this.state.EmployeeData.map(Employee => (
                <div className="EmployeeRow_Row" key={"EmployeeRoster__" + Employee.id} style={GetRowWidth()}>
                  {Employee.shifts.map(Shift => (
                    <div onClick={this.handleEditClick(Employee.id, Shift.id)} style={getRosterStyling(Shift)} key={Employee.id + '_' + Shift.id} className="RosterEntry">
                      {GetMoment(Shift.start_time).format("dddd - h:mmA") + ' to ' + GetMoment(Shift.end_time).format("h:mmA")}
                    </div>
                  ))}
                </div>
              ))}
            </div>
          </div>
        </div>

        <p>To edit click on a roster entry and a modal will appear</p>
      </div>
    );
  }

  renderTable() {
    return (
      <div className="TableView">
        <div className="TableHeader">
          {(() => {      
            const StartPoint = WeekStart.clone(); // For some reason .add is a mutative function ?

            return (Array.apply(null, Array(7))).map((_, Index) => (
              <div className="TableHeader__Entry" key={"Day" + Index}>
                {StartPoint.add(((Index === 0) ? 0 : 1), 'day').format('dddd')}
              </div>
            ))
          })()}
        </div>
        <div className="TableContent">
          {this.state.EmployeeData.map(Employee => {
            const Days = Employee.shifts.reduce((Total, Shift) => {
              const WeekStartUnix = WeekStart.unix();
              const StartUnix = GetMoment(Shift.start_time).unix();
              const StartDay = Math.floor((StartUnix - WeekStartUnix)/(60*60*24));

              return {
                ...Total,
                [StartDay]: [
                  ...(Total.StartDay || []),
                  Shift,
                ],
              }
            }, {});

            return (
              <div className="Employee" key={"Employee__" + Employee.id}>
                <div className="Employee__Contact">
                  {Employee.first_name + " " + Employee.last_name}
                </div>
                {(Array.apply(null, Array(7))).map((_, Index) => {
                  const DayShifts = Days[Index] || [];

                  return (
                    <div className="EmployeeDayEntry" key={"Day_" + Employee.id + "_" + Index}>
                      {DayShifts.map(Shift => (
                        <Fragment key={Shift.id + '__Entry'}>
                          <div className="Start">
                            <input onChange={this.handleTableEdit(Employee.id)(Shift.id)('start_time')} type="text" defaultValue={GetMoment(Shift.start_time).format("h:mmA")}/>
                          </div>
                          <div className="End">
                            <input onChange={this.handleTableEdit(Employee.id)(Shift.id)('start_time')} type="text" defaultValue={GetMoment(Shift.end_time).format("h:mmA")}/>
                          </div>
                        </Fragment>
                      ))}
                    </div>
                  )
                })}
              </div>
            )
          })}
        </div>

        <p>To edit just click on a field value</p>
      </div>
    )
  }

  render() {
    return (
      <div>
        {this.state.EmployeeData && ((this.state.DisplayTable === false) ? this.renderVisual() : this.renderTable())}
        <br/>
        <button onClick={this.toggleDispalyView}>
          Change View
        </button>
      </div>
    );
  }
}

export default App;
